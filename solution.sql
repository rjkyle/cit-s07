Microsoft Windows [Version 10.0.22621.1265]
(c) Microsoft Corporation. All rights reserved.

C:\Users\Kyle>mysql -u root
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 117
Server version: 10.4.25-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> USE blog_db
Database changed
MariaDB [blog_db]> INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com","passwordA", "2021-01-01 01:00:00");
Query OK, 1 row affected (0.001 sec)

MariaDB [blog_db]> INSERT INTO users (email, password, datetime_created) VALUES ("juandelecruz@gmail.com","passwordB", "2021-01-01 02:00:00");
Query OK, 1 row affected (0.003 sec)

MariaDB [blog_db]> INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com","passwordC", "2021-01-01 03:00:00");
Query OK, 1 row affected (0.003 sec)

MariaDB [blog_db]> INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com","passwordD", "2021-01-01 04:00:00");
Query OK, 1 row affected (0.003 sec)

MariaDB [blog_db]> INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com","passwordE", "2021-01-01 05:00:00");
Query OK, 1 row affected (0.001 sec)

MariaDB [blog_db]> SELECT * FROM users;
+----+-------------------------+-----------+---------------------+
| id | email                   | password  | datetime_created    |
+----+-------------------------+-----------+---------------------+
|  1 | johnsmith@gmail.com     | passwordA | 2021-01-01 01:00:00 |
|  2 | juandelecruz@gmail.com  | passwordB | 2021-01-01 02:00:00 |
|  3 | janesmith@gmail.com     | passwordC | 2021-01-01 03:00:00 |
|  4 | mariadelacruz@gmail.com | passwordD | 2021-01-01 04:00:00 |
|  5 | johndoe@gmail.com       | passwordE | 2021-01-01 05:00:00 |
+----+-------------------------+-----------+---------------------+
5 rows in set (0.000 sec)

MariaDB [blog_db]> INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World", "2021-01-02 01:00:00");
Query OK, 1 row affected (0.004 sec)

MariaDB [blog_db]> DESCRIBE posts;
+-----------------+--------------+------+-----+---------+----------------+
| Field           | Type         | Null | Key | Default | Extra          |
+-----------------+--------------+------+-----+---------+----------------+
| id              | int(11)      | NO   | PRI | NULL    | auto_increment |
| author_id       | int(11)      | NO   | MUL | NULL    |                |
| content         | varchar(500) | YES  |     | NULL    |                |
| datetime_posted | datetime     | YES  |     | NULL    |                |
| title           | varchar(500) | YES  |     | NULL    |                |
+-----------------+--------------+------+-----+---------+----------------+
5 rows in set (0.007 sec)

MariaDB [blog_db]> ALTER TABLE posts ADD COLUMN title VARCHAR(500) AFTER author_id;
ERROR 1060 (42S21): Duplicate column name 'title'
MariaDB [blog_db]> DESCRIBE posts;
+-----------------+--------------+------+-----+---------+----------------+
| Field           | Type         | Null | Key | Default | Extra          |
+-----------------+--------------+------+-----+---------+----------------+
| id              | int(11)      | NO   | PRI | NULL    | auto_increment |
| author_id       | int(11)      | NO   | MUL | NULL    |                |
| content         | varchar(500) | YES  |     | NULL    |                |
| datetime_posted | datetime     | YES  |     | NULL    |                |
| title           | varchar(500) | YES  |     | NULL    |                |
+-----------------+--------------+------+-----+---------+----------------+
5 rows in set (0.008 sec)

MariaDB [blog_db]> INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World", "2021-01-02 01:00:00");
Query OK, 1 row affected (0.003 sec)

MariaDB [blog_db]> INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth", "2021-01-02 02:00:00");
Query OK, 1 row affected (0.001 sec)

MariaDB [blog_db]> INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");
Query OK, 1 row affected (0.001 sec)

MariaDB [blog_db]> INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye bye Solar System!", "2021-01-02 04:00:00");
Query OK, 1 row affected (0.003 sec)

MariaDB [blog_db]> SELECT * FROM posts;
+----+-----------+-----------------------+---------------------+-------------+
| id | author_id | content               | datetime_posted     | title       |
+----+-----------+-----------------------+---------------------+-------------+
|  1 |         1 | Hello World           | 2021-01-02 01:00:00 | First Code  |
|  2 |         1 | Hello World           | 2021-01-02 01:00:00 | First Code  |
|  3 |         1 | Hello Earth           | 2021-01-02 02:00:00 | Second Code |
|  4 |         2 | Welcome to Mars!      | 2021-01-02 03:00:00 | Third Code  |
|  5 |         4 | Bye bye Solar System! | 2021-01-02 04:00:00 | Fourth Code |
+----+-----------+-----------------------+---------------------+-------------+
5 rows in set (0.000 sec)

MariaDB [blog_db]> SELECT * FROM posts WHERE author_id = 1;
+----+-----------+-------------+---------------------+-------------+
| id | author_id | content     | datetime_posted     | title       |
+----+-----------+-------------+---------------------+-------------+
|  1 |         1 | Hello World | 2021-01-02 01:00:00 | First Code  |
|  2 |         1 | Hello World | 2021-01-02 01:00:00 | First Code  |
|  3 |         1 | Hello Earth | 2021-01-02 02:00:00 | Second Code |
+----+-----------+-------------+---------------------+-------------+
3 rows in set (0.000 sec)

MariaDB [blog_db]> SELECT email, datetime_created FROM users;
+-------------------------+---------------------+
| email                   | datetime_created    |
+-------------------------+---------------------+
| johnsmith@gmail.com     | 2021-01-01 01:00:00 |
| juandelecruz@gmail.com  | 2021-01-01 02:00:00 |
| janesmith@gmail.com     | 2021-01-01 03:00:00 |
| mariadelacruz@gmail.com | 2021-01-01 04:00:00 |
| johndoe@gmail.com       | 2021-01-01 05:00:00 |
+-------------------------+---------------------+
5 rows in set (0.000 sec)

MariaDB [blog_db]> UPDATE posts SET content = "Hello to the People of the Earth!" WHERE id = 2;
Query OK, 1 row affected (0.003 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [blog_db]> SELECT * FROM posts;
+----+-----------+-----------------------------------+---------------------+-------------+
| id | author_id | content                           | datetime_posted     | title       |
+----+-----------+-----------------------------------+---------------------+-------------+
|  1 |         1 | Hello World                       | 2021-01-02 01:00:00 | First Code  |
|  2 |         1 | Hello to the People of the Earth! | 2021-01-02 01:00:00 | First Code  |
|  3 |         1 | Hello Earth                       | 2021-01-02 02:00:00 | Second Code |
|  4 |         2 | Welcome to Mars!                  | 2021-01-02 03:00:00 | Third Code  |
|  5 |         4 | Bye bye Solar System!             | 2021-01-02 04:00:00 | Fourth Code |
+----+-----------+-----------------------------------+---------------------+-------------+
5 rows in set (0.000 sec)

MariaDB [blog_db]> DELETE FROM users WHERE email = "johndoe@gmail.com";
Query OK, 1 row affected (0.004 sec)

MariaDB [blog_db]> SELECT * FROM users;
+----+-------------------------+-----------+---------------------+
| id | email                   | password  | datetime_created    |
+----+-------------------------+-----------+---------------------+
|  1 | johnsmith@gmail.com     | passwordA | 2021-01-01 01:00:00 |
|  2 | juandelecruz@gmail.com  | passwordB | 2021-01-01 02:00:00 |
|  3 | janesmith@gmail.com     | passwordC | 2021-01-01 03:00:00 |
|  4 | mariadelacruz@gmail.com | passwordD | 2021-01-01 04:00:00 |
+----+-------------------------+-----------+---------------------+
4 rows in set (0.000 sec)

MariaDB [blog_db]>